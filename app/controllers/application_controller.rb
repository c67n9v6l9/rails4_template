# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  layout :layout_handler

  def render_404(e = nil)
    render :template => 'errors/404', :status => 404
  end

  def render_500(e = nil)
    logger.error e.message if e.present?
    render :template => 'errors/500', :status => 500
  end

  unless Rails.env.development?
    rescue_from Exception,                      :with => :render_500
    rescue_from ActiveRecord::RecordNotFound,   :with => :render_404
    rescue_from ActionController::RoutingError, :with => :render_404
  end

  private
  def admin?
    request.path =~ /^\/admin/
  end

  def layout_handler
    admin? ? 'admin' : 'application'
  end
end
