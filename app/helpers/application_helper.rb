module ApplicationHelper
  def default_meta_tags
    {
      reverse: true,
      site: 'site',
      title: 'title',
      description: 'description',
      keywords: 'keywords',
      og: {
        title: :title,
        type: 'website',
        url: "url",
        image: "url",
        site_name: :title,
        description: :description,
        locale: 'ja_JP'
      },
      twitter: {
        card: 'summary_large_image',
        site: '@site',
        title: :title,
        description: :description,
        image: {
          src: "url"
        }
      },
      charset: 'UTF-8'
    }
  end

  def is_error(object, param)
    return nil if object == nil || param == nil
    return !object.errors[param].length.zero? && 'is-error'
  end

  def hbr(str)
    str = html_escape(str)
    str.gsub(/\r\n|\r|\n/, '<br />').html_safe
  end

  def nobr(str)
    str = html_escape(str)
    str.gsub(/\r\n|\r|\n/,'').html_safe
  end

  def replace_link(text)
    text.gsub(/https?:\/\/(\w*[a-zA-ZＡ-Ｚａ-ｚ0-9０-９ぁ-ヶ亜-黑\.\/\?\#\%\&]+)/) do |t|
      text = text.sub(Regexp.new(t),"<a href=\"#{t}\" target=\"_blank\">#{t}</a>")
    end

    text.gsub(/[#＃](\w*[a-zA-ZＡ-Ｚａ-ｚ0-9０-９ぁ-ヶ亜-黑]+)/) do |t|
      text = text.sub(Regexp.new(t),"<a href=\"#{Settings.twitter.url_search_hashtag}#{t.slice(1..-1)}\" target=\"_blank\">#{t}</a>")
    end

    return text.html_safe
  end

end
