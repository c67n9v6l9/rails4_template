#
# Application Template
#

repo_url = 'https://bitbucket.org/c67n9v6l9/rails4_template/raw/master'
gems = {}

@app_name = app_name

def get_and_gsub(source_path, local_path)
  get source_path, local_path

  gsub_file local_path, /%app_name%/, @app_name
  gsub_file local_path, /%app_name_classify%/, @app_name.classify
end

#
# Gemfile
#

remove_file 'Gemfile'
get "#{repo_url}/Gemfile", 'Gemfile'

# jpmobile
if yes? 'Would you like to use jpmobile ?'
  gem 'jpmobile'
end

# ImageMagick
if yes? 'Would you like to use rmagick & carrierwave ?'
  gem 'rmagick', require: 'RMagick'
  gem 'carrierwave'
  gem 'fog'
end

# Admin
gems[:admin] = yes? 'Would you like to use admin ?'
if gems[:admin]
  gems[:active_admin] = yes? 'Would you like to use active_admin ?'
  if gems[:active_admin]
    gem 'activeadmin', :github => 'gregbell/active_admin'
    gem 'devise'
    gem 'just-datetime-picker'
  else
    gem "will_paginate"
    gem "twitter-bootstrap-rails",
      :git => "git://github.com/seyhunak/twitter-bootstrap-rails",
      :branch => "bootstrap3"
    gem "less-rails"
    gem "devise"
  end
end

# Template Engine
@template_engine = ask 'What do you want to use template engine?'
case @template_engine
when 'haml'
  gem 'haml-rails'
  gem 'erb2haml'
when 'slim'
  gem 'slim-rails'
  gem 'html2slim'
end

#
# bundle install
#

run 'bundle install --path=./vendor/bundle'

#
# gitignore
#

remove_file '.gitignore'
get "#{repo_url}/gitignore", '.gitignore'

#
# app
#

# controllers
remove_file 'app/controllers/application_controller.rb'
get "#{repo_url}/app/controllers/application_controller.rb", 'app/controllers/application_controller.rb'

# helpers
remove_file 'app/helpers/application_helper.rb'
get "#{repo_url}/app/helpers/application_helper.rb", 'app/helpers/application_helper.rb'

# views
remove_file 'app/views/layouts/application.html.erb'
get "#{repo_url}/app/views/layouts/application.html.erb", 'app/views/layouts/application.html.erb'

case @template_engine
when 'haml'
  rake 'haml:replace_erbs'
when 'slim'
  run 'for i in app/views/**/*.erb; do ./bin/bundle exec erb2slim $i ${i%erb}slim && rm $i; done'
end

#
# config
#

# database
remove_file 'config/database.yml'
get_and_gsub "#{repo_url}/config/database.yml", 'config/database.yml'
get_and_gsub "#{repo_url}/config/database.yml", 'config/database.yml.sample'

# unicorn
get_and_gsub "#{repo_url}/config/unicorn.rb", 'config/unicorn.rb'

# application
get_and_gsub "#{repo_url}/config/application.rb",'config/application.rb'

#
# generator
#

# config
generate 'config:install'

#
# rake
#

rake 'db:create'
rake 'db:migrate'
