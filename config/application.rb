require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(*Rails.groups)

module %app_name_classify%
  class Application < Rails::Application
    config.autoload_paths += Dir[Rails.root.join('lib')]
    config.autoload_paths += Dir[Rails.root.join('app', 'models')]

    config.time_zone = 'Tokyo'

    config.i18n.enforce_available_locales = false
    config.i18n.default_locale = :ja
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}').to_s]

    config.active_record.default_timezone = :local
  end
end
